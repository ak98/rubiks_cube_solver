import functions
import random

# D2 U F L' B' D' R' U R L' B2 D F B D L R2 B F' U F U L' B' U
sample1 = ["rbwrrwbgo", "ryyrgybbg", "bgggogrwo", "wogwbbgry", "ooryybwwb", "orwywoyoy"]

# U2 R' F2 B2 U L U F R2 D2 L' F R' L F' D2 R U' D' B2 L2 D R B' U2
sample2 = ["yowyroggy", "rrwwgbbwg", "bowyowywy", "grgbbgrrw", "rbogyyryb", "ooobwrbgo"]

sample3 = ["rrrrrrrro", "ggggggygg", "obroooooo", "wobbbbbbb", "gyyyyyyyy", "wwbwwwwww"]
sample4 = ["rrrrrrgro", "ggggggygg", "ooboooooo", "wbbbbbbbr", "ryyyyyyyy", "wwbwwwwww"]
sample5 = ["rrrrrrrrr", "ggggggggg", "ooooooooo", "bbbbbbbbb", "yyyyyyyyy", "wwwwwwwww"]

cube = functions.generate_cube(sample2)

counter = 0
print("**** Corners solving ****")
while not functions.pieces_finished(cube, functions.corners):
    print("\r\n" + str(counter + 1) + ".")
    counter += 1
    unsolved_corners = functions.unsolved(cube, functions.corners)
    buffer_destination = functions.get_corner_buffer_destination(cube)
    location = functions.get_a_destination(cube)
    if location not in ["A", "N", "Q"]:
        functions.print_corner_instructions(location)
        functions.rewrite_corner(functions.corner_replacement_scheme[location], cube)
        unsolved_corners.remove(set(buffer_destination))
    else:
        random_corner = list(random.choice([corner for corner in unsolved_corners if corner != {'A', 'N', 'Q'}]))
        location = random_corner[0]
        functions.print_corner_instructions(location)
        functions.rewrite_corner(functions.corner_replacement_scheme[location], cube)

print("\nNeeded:",counter, "iterations. Corners are solved!")

counter = 0
print("\n**** Edges solving ****")
while not functions.pieces_finished(cube, functions.edges):
    print("\r\n" + str(counter + 1) + ".")
    counter += 1
    unsolved_edges = functions.unsolved(cube, functions.edges)
    buffer_destination = functions.get_edge_buffer_destination(cube)
    location = functions.get_b_destination(cube)
    if location not in ["b", "i"]:
        functions.print_edge_instructions(location)
        functions.rewrite_edge(functions.edge_replacement_scheme[location], cube)
        unsolved_edges.remove(set(buffer_destination))
    else:
        random_edge = list(random.choice([edge for edge in unsolved_edges if edge != {'b', 'i'}]))
        location = random_edge[0]
        functions.print_edge_instructions(location)
        functions.rewrite_edge(functions.edge_replacement_scheme[location], cube)

print("\nNeeded:", counter, "iterations. Edges are solved!")