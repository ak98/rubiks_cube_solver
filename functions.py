#cube
red = "EeFh*fHgG"
yellow = "AaBd*bDcC"
green = "IiJl*jLkK"
orange = "MmNp*nPoO"
blue = "QqRt*rTsS"
white = "UuVx*vXwW"

corners = "ABCDEFGHIJKLMNOPQRSTUVWX"
corner_switch_alg = "R U' R' U' R U R' F' R U R' U' R' F R "
corner_setup_moves = {'A': 'Bank place', 'B': "R D'", 'C': 'F', 'D': "F R'", 'E': "F' D", 'F': 'F2 D', 'G': 'D R',
                      'H': 'D','I': "R'", 'J': 'R2', 'K': 'R', 'L': 'None', 'M': "R' F", 'N': 'Bank place',
                      'O': "D' R", 'P': "D'", 'Q': 'Bank place', 'R': 'F2', 'S': 'D2 R', 'T': 'D2', 'U': "F'",
                      'V': "D' F'", 'W': "D2 F'", 'X': "D F'"}

corner_groups = [{'E', 'R', 'D'}, {'H', 'S', 'U'}, {'O', 'T', 'X'}, {'A', 'Q', 'N'}, {'W', 'P', 'K'}, {'L', 'V', 'G'},
                 {'M', 'J', 'B'}, {'I', 'C', 'F'}]

corner_destinations = {'DRE': {'y', 'b', 'r'}, 'SHU': {'b', 'r', 'w'}, 'TOX': {'b', 'w', 'o'}, 'AQN': {'y', 'b', 'o'},
                       'WKP': {'g', 'w', 'o'}, 'LGV': {'r', 'g', 'w'}, 'BJM': {'y', 'g', 'o'}, 'CFI': {'y', 'r', 'g'}}

corner_replacement_scheme = {"A": None, "B": "BMJ", "C": "CIF", "D": "DER", "E": "ERD", "F": "FCI", "G": "GLV",
                             "H": "HUS", "I": "IFC", "J": "JBM", "K": "KPW", "L": "LVG", "M": "MJB",
                            "N": None, "O": "OTX", "P": "PWK", "Q": None, "R": "RDE", "S": "SHU",
                            "T": "TXO", "U": "USH", "V": "VGL", "W": "WKP", "X": "XOT"}

edges = corners.lower()
j_perm = "R U R' F' R U R' U' R' F R2 U' R' U'"
t_perm = "R U R' U' R' F R2 U' R' U' R U R' F'"
edge_setup_moves = {'a': "l2 D' L2 (T)", 'b': None, 'c': None, 'd': None, 'e': "l D' L2 (T)", 'f': 'd2 L (T)', 'g': "l' (J)",
                          'h': "L' (T)", 'i': None, 'j': "d L (T)", 'k': "D' l' (J)", 'l': "d' L' (T)", 'm': "l (J)", 'n': "L (T)",
                      'o': "D2 l' (J)", 'p': "d2 L' (T)", 'q': "L2 d l' (J)", 'r': "d' L (T)", 's': "D l' (J)", 't': "d L' (T)", 'u': "D' L2 (T)",
                      'v': "D2 L2 (T)", 'w': "D L2 (T)", 'x': "L2 (T)"}

edge_groups = [{'e', 'c'}, {'r', 'h'}, {'l', 'f'}, {'u', 'g'}, {'b', 'i'}, {'k', 'v'}, {'p', 'j'}, {'m', 'a'},
               {'w', 'o'}, {'t', 'n'}, {'q', 'd'}, {'s', 'x'}]

edge_destinations = {"ce": {'y', 'r'}, "rh": {'b', 'r'}, "fl": {'g', 'r'}, "gu": {'w', 'r'}, "bi": {'y', 'g'},
                     "kv": {'w', 'g'}, "jp": {'o', 'g'}, "ma": {'y', 'o'}, "ow": {'o', 'w'}, "nt": {'o', 'b'},
                     "qd": {'y', 'b'}, "sx": {'w', 'b'}}

edge_replacement_scheme = {"a": "am", "b": None, "c": "ce", "d": "dq", "e": "ec", "f": "fl", "g": "gu",
                             "h": "hr", "i": None, "j": "jp", "k": "kv", "l": "lf", "m": "ma",
                            "n": "nt", "o": "ow", "p": "pj", "q": "qd", "r": "rh", "s": "sx",
                            "t": "tn", "u": "ug", "v": "vk", "w": "wo", "x": "xs"}
# input functionality
def corner_group_color(corner_group, cube):
    aux_list = []
    for item in cube.values():
        for key, value in item.items():
            for char in corner_group:
                if char == key:
                    aux_list.append(value)
                    break
    return  aux_list

def check_side_from_input(string):

    allowed_values = ["y", "w", "g", "b", "r", "o"]

    if type(string) is not str or len(string) != 9:
        raise Exception("Supplied value is not length of 9")

    for char in string:
        if char not in allowed_values:
            raise Exception("Characters not allowed")

    return string

def generate_cube(input):
    checked = []
    for _ in input:
        checked.extend(input)

    cube = {}
    for item in checked:
        positions = {}
        if item[4] == "r":
            for i in range(len(item)):
                positions[red[i]] = item[i]
            cube["r"] = positions
        elif item[4] == "g":
            for i in range(len(item)):
                positions[green[i]] = item[i]
            cube["g"] = positions
        elif item[4] == "y":
            for i in range(len(item)):
                positions[yellow[i]] = item[i]
            cube["y"] = positions
        elif item[4] == "o":
            for i in range(len(item)):
                positions[orange[i]] = item[i]
            cube["o"] = positions
        elif item[4] == "b":
            for i in range(len(item)):
                positions[blue[i]] = item[i]
            cube["b"] = positions
        elif item[4] == "w":
            for i in range(len(item)):
                positions[white[i]] = item[i]
            cube["w"] = positions
    return cube

# checking for unsolved ccorners and checking if all corners are solved
def unsolved(cube, array):
    if array == corners:
        groups = corner_groups
    else:
        groups = edge_groups

    unsolved_list = []
    for side, content in cube.items():
        for key, value in content.items():
            if key in array and value != side:
                unsolved_list.append(key)

    unsolved_groups = []
    for char in unsolved_list:
        for binding in groups:
            if char in binding and binding not in unsolved_groups:
                unsolved_groups.append(binding)

    return unsolved_groups

def pieces_finished(cube, array):
    for location in array:
        side = get_side(location)
        if side != cube[side][location]:
            return False

    return True

#solving functions
def invert_setup(setup):
    list_of_turns = setup.split()
    inverted_list = []
    for turn in reversed(list_of_turns):
        if "'" in turn:
            inverted_list.append(turn[:-1])
        else:
            inverted_list.append(turn + "'")
    return " ".join(inverted_list)

def get_current_color_of(location, cube):
    for side in cube.values():
        for loc,color in side.items():
            if loc == location:
                return color

def get_corner_buffer_destination(cube):
    my_set = {get_current_color_of("A", cube), get_current_color_of("N", cube), get_current_color_of("Q", cube)}
    for key, value in corner_destinations.items():
        if value == my_set:
            return key

def get_edge_buffer_destination(cube):
    my_set = {get_current_color_of("b", cube), get_current_color_of("i", cube)}
    for key, value in edge_destinations.items():
        if value == my_set:
            return key

def get_a_destination(cube):
    piece_destination = get_corner_buffer_destination(cube)
    color_of_A = get_current_color_of("A", cube)
    for loc in cube[color_of_A].keys():
        if loc in piece_destination:
            return loc

def get_b_destination(cube):
    piece_destination = get_edge_buffer_destination(cube)
    color_of_b = get_current_color_of("b", cube)
    for loc in cube[color_of_b].keys():
        if loc in piece_destination:
            return loc

def print_corner_instructions(location):
    print("Location to move to: ", location)
    print("Setup moves: ", corner_setup_moves[location])
    print("Carry out replacing alg.: ", corner_switch_alg)
    print("Undo setup moves by doing: ", invert_setup(corner_setup_moves[location]))

def print_edge_instructions(location):
    print("Location to move to: ", location)

    if edge_setup_moves[location]:
        print("Setup moves: ", edge_setup_moves[location][:-3])
        if "J" in edge_setup_moves[location]:
            print("Carry out replacing alg.: ", j_perm)
        else:
            print("Carry out replacing alg.: ", t_perm)
        print("Undo setup moves by doing: ", invert_setup(edge_setup_moves[location][:-3]))
    else:
        print("Setup moves: ", edge_setup_moves[location])
        if location == 'c':
            print("Carry out replacing alg.: ", j_perm)
        else:
            print("Carry out replacing alg.: ", t_perm)

        print("Undo setup moves by doing: ", None)


def get_side(location):
    cube = [{"red" : "EeFh*fHgG"}, {"yellow" : "AaBd*bDcC"}, {"green" : "IiJl*jLkK"}, {"orange" : "MmNp*nPoO"},
    {"blue" : "QqRt*rTsS"}, {"white" : "UuVx*vXwW"}]
    for side in cube:
        for side_name, locations in side.items():
            if location in locations:
                return side_name[0]

def rewrite_corner(location, cube):
    a = get_current_color_of("A", cube)
    q = get_current_color_of("Q", cube)
    n = get_current_color_of("N", cube)

    x = get_current_color_of(location[0], cube)
    y = get_current_color_of(location[1], cube)
    z = get_current_color_of(location[2], cube)

    cube[get_side(location[0])][location[0]] = a
    cube[get_side(location[1])][location[1]] = q
    cube[get_side(location[2])][location[2]] = n

    cube[get_side("A")]["A"] = x
    cube[get_side("Q")]["Q"] = y
    cube[get_side("N")]["N"] = z

    #edges
    value_m = cube["o"]['m']
    value_q = cube["b"]['q']
    value_a = cube["y"]["a"]
    value_d = cube["y"]["d"]

    cube["o"]['m'] = value_q
    cube["b"]['q'] = value_m
    cube["y"]["a"] = value_d
    cube["y"]["d"] = value_a


def rewrite_edge(location, cube):

    b = get_current_color_of("b", cube)
    i = get_current_color_of("i", cube)

    x = get_current_color_of(location[0], cube)
    y = get_current_color_of(location[1], cube)

    cube[get_side(location[0])][location[0]] = b
    cube[get_side(location[1])][location[1]] = i

    cube[get_side("b")]["b"] = x
    cube[get_side("i")]["i"] = y

    # corners
    value_f = cube["r"]['F']
    value_i = cube["g"]['I']
    value_j = cube["g"]["J"]
    value_m = cube["o"]["M"]

    cube["r"]['F'] = value_j
    cube["g"]['I'] = value_m
    cube["g"]["J"] = value_f
    cube["o"]["M"] = value_i
