import functions

dictionary = {"r": "red", "g": "green", "w": "white", "y": "yellow", "b": "blue", "o": "orange"}


class Side:

    def __init__(self, value):
        self.fields = functions.generate_side_from_input(value)
        self.color = self.fields[1][1]

    def get_name(self):
        return dictionary[self.color]

    def get_color(self):
        return self.color

    def print_fields(self):
        for row in self.fields:
            print(row)

    def turn_r(self):
        old_fields = self.fields
        self.fields = [[self.fields[2][0], self.fields[1][0], self.fields[0][0]],
                       [self.fields]]

cube = [Side(value) for value in functions.sample]


side = Side("goggrbowr")

print("side: ", side.print_fields())
print("side: ", side.fields)
#print(cube.sides)
#print(cube.yellow_side)